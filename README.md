# Policy UI
The policy UI is a user interface checking the content of a policy service. It is intended for demo purposes

## Running the app

### Run using Skaffold
Install microk8s according to [Microk8s installation](https://microk8s.io/docs/install-alternatives)
Install Helm according to [Helm installation](https://helm.sh/docs/intro/install/)
Install Skaffold according to [Skaffold installation](https://skaffold.dev/docs/install/)
Copy the values from `microk8s.kubectl config view` to $HOME/.kube/config

```bash
docker login registry.gitlab.com # a gitlab access token can be used as password here

kubectl config use-context microk8s
kubectl create ns dvi-emergency-center
kubectl create ns dvi-digital-home-001
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-emergency-center
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-digital-home-001
```
This `regcred` secret will be used by the Helm deployment to fetch the Docker image. Make sure to create it in the correct namespace.

Alternatively, it is also possible to create the regcred using:
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-emergency-center
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-digital-home-001
```

Then run:

```bash
skaffold dev --cleanup=false -p emergency-center
```

It may take some time for all resources to become healthy.

To test that the policy-api is up, make sure you can connect to it, for example using a port-forward

```bash
kubectl port-forward service/policy-ui 8080:http --namespace=dvi-emergency-center
curl localhost:8080/health
```

To run the digital house policy service:
```bash
skaffold dev --cleanup=false -p digital-home

kubectl port-forward service/policy-ui 8080:http --namespace=dvi-digital-home-001
curl localhost:8080/health
```

## License
See [LICENSE.md](LICENSE.md)

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
