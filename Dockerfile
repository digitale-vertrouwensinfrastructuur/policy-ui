# Set the base image to node:12-alpine
FROM node:16-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN yarn install
RUN yarn global add react-scripts@4.0.3
# We want the production version
RUN yarn build

# Prepare nginx
FROM nginx:1.19.10-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

WORKDIR /etc/nginx
COPY ./nginx/env_template.js ./env_template.js
COPY ./nginx/substituteEnv.sh ./substituteEnv.sh
RUN chmod 777 ./substituteEnv.sh

# Make sure nginx runs with deamon off
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Fire up nginx
EXPOSE 80
# Execute the subsitution script and pass the path of the file to replace
ENTRYPOINT [ "./substituteEnv.sh", "/etc/nginx/env_template.js", "/usr/share/nginx/html/env.js"]

# Start nginx. Note: this actually not used, nginx is started by the substitution script
CMD ["nginx"]
