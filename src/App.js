import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, { useState, useEffect } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';

function App() {
  /*global APPENV */
  const policyOrganization = APPENV.POLICY_SERVICE_ORGANIZATION;

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [policies, setPolicies] = useState([]);
  
  // const dummyPolicies = [{"policy_id":91,"actor":"Dummy","action":"read:someinformation","actee":{"actor":"Digitale Huis X","url":"http://localhost:8081/home/x"},"conditions":[{"type":"calamity","authority":"some-authority","assertion":"Because Simon says so"}],"goal":"to test this","Signature":null}];

  useEffect(() => {
    loadPolicies();
  }, []);

  function loadPolicies() {
    return fetch(APPENV.POLICY_SERVICE_URL)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result);
        setIsLoaded(true);
        setPolicies(result);
      },
      (error) => {
        console.log(error);
        setIsLoaded(true);
        setError(error);
      }
    )
  }

  function deletePolicy(policyId) {
    return fetch(APPENV.POLICY_SERVICE_URL + '/' + policyId, { method: 'DELETE'});
  }

  function deletePolicyAndRefresh(policyId) {
    deletePolicy(policyId)
      .then(loadPolicies);
  };

  return isLoaded ? (
    <Container>
      <Row style={{margin: "10px"}}>
        <Col><h3 style={{color: '#60cda2'}}>Toestemmingen ingesteld bij {policyOrganization}</h3></Col>
      </Row>
      <Row className="justify-content-md-center">
        <Col>
          <Table striped bordered hover style={{background: 'white'}}>
            <thead>
              <tr>
                <th>#</th>
                <th>Actor</th>
                <th>Actie</th>
                <th>Actee</th>
                <th>Condities</th>
                <th>Doel</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
                {policies.map(p => (
                  <tr key={p.policy_id}>
                    <td>{p.policy_id}</td>
                    <td>{p.actor}</td>
                    <td>{p.action.scope}</td>
                    <td>{p.actee.actor}</td>
                    <td>{p.conditions.map(c => c.description).join(", ")}</td>
                    <td>{p.goal}</td>
                    <td><Button onClick={() => deletePolicyAndRefresh(p.policy_id)}>Verwijderen</Button></td>
                  </tr>
                ))}
                {policies.length === 0 ? (
                  <tr>
                    <td colspan="7" style={{textAlign: 'center'}}>Geen toestemmingen ingesteld.</td>
                  </tr>
                ) : ""}
            </tbody>
          </Table>     
        </Col>
      </Row>
      <Row style={{margin: "10px", color: "white"}}>Error: {error ? error.toString() : ""}</Row>
    </Container>
  ) : ("Bezig met laden...");
}

export default App;
